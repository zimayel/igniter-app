/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airhacks.followme;

import javafx.application.Application;
import javafx.application.Preloader.PreloaderNotification;



/**
 *
 * @author Zied
 */
public class MessageNotification implements PreloaderNotification{
    /** The message. */
    private final String message;

    /** The application. */
    private final Application application;

    /**
     * Instantiates a new message notification.
     *
     * @param message the message
     * @param application the application that triggers this message
     */
    public MessageNotification(final String message, final Application application) {
        super();
        this.message = message;
        this.application = application;
    }

    /**
     * Gets the message.
     *
     * @return Returns the message.
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Gets the application.
     *
     * @return Returns the application.
     */
    public Application getApplication() {
        return this.application;
    }
}
