package com.airhacks.followme.dashboard;

import com.airhacks.followme.commons.ImageNames;
import com.airhacks.followme.dashboard.light.LightView;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javax.inject.Inject;

/**
 *
 * @author adam-bien.com
 */
public class DashboardPresenter implements Initializable {

    @FXML Button menuBtn;
    @FXML BorderPane borderPane;

    @Inject
    Tower tower;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        menuBtn.setGraphic(new ImageView(new Image(ImageNames.LAUNCH.Path())));
        
    }

    

}
