package com.airhacks.followme;

/*
 * #%L
 * igniter
 * %%
 * Copyright (C) 2013 - 2016 Adam Bien
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.airhacks.followme.dashboard.DashboardView;
import com.airhacks.afterburner.injection.Injector;
import com.airhacks.followme.demo.DemoPresenter;
import com.airhacks.followme.demo.DemoView;
import java.time.LocalDate;
import java.time.Month;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.application.Preloader;
import javafx.concurrent.Task;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author adam-bien.com
 */
public class App extends AbstractApplication<StackPane> {

    @Override
    public void start(Stage stage) throws Exception {
        /*
         * Properties of any type can be easily injected.
         */
        LocalDate date = LocalDate.of(4242, Month.JULY, 21);
        Map<Object, Object> customProperties = new HashMap<>();
        customProperties.put("date", date);
        /*
         * any function which accepts an Object as key and returns
         * and return an Object as result can be used as source.
         */
        Injector.setConfigurationSource(customProperties::get);

        System.setProperty("happyEnding", " Enjoy the flight!");
        DashboardView appView = new DashboardView();
        Scene scene = new Scene(appView.getView());
        stage.setTitle("followme.fx");
        final String uri = getClass().getResource("app.css").toExternalForm();
        scene.getStylesheets().add(uri);
        stage.setScene(scene);

        stage.show();
    }

    @Override
    public void stop() throws Exception {
        Injector.forgetAll();
    }

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            preloadAndLaunch(SakilaPreloader.class, args);
        });
        thread.setName("APP THREAD");
        thread.setDaemon(true);
        thread.start();

    }

    @Override
    protected void preInit() {

        //initDb();
        dbInitThread initThread = new dbInitThread(this);
        initThread.run();
        while (initThread.running) {
            try {
                System.out.println("wait to change state");
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            this.initBla();
            for (int i = 0; i < 11; i++) {
                notifyPreloader(new Preloader.ProgressNotification(i * (0.1)));
                String msg = String.format(" LOOP : %1s", i + 1);
                MessageNotification notification = new MessageNotification(String.valueOf((i + 1) * 100), this);
                notifyPreloader(notification);
                System.out.println(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (Exception e) {
        }

        System.out.println("com.airhacks.followme.App.preInit()");

    }

    private static boolean dbInitialized;
    DemoPresenter demoPresenter;

    private void initBla() {

    }

    @Override
    protected void postInit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    class dbInitThread implements Runnable {

        private volatile boolean running;
        private final Application application;

        public dbInitThread(Application app) {
            this.running = true;
            application = app;
        }

        @Override
        public void run() {
            if (!dbInitialized) {
                Platform.runLater(() -> {
                    Stage stage = new Stage();
                    DemoView demoView = new DemoView();
                    demoPresenter = (DemoPresenter) demoView.getPresenter();
                    //
                    // register the save event
                    //
                    demoPresenter.getSave().setOnAction((event) -> {

                        // on save 
                        //TODO check if configuration is saved correctely
                        // to be set after check
                        stop();
                        ((Node) event.getSource()).getScene().getWindow().hide();

                    });
                    //
                    // register the cancel event
                    //
                    demoPresenter.getExit().setOnAction((event) -> {
                        //release the Process
                        //TODO CLOSE the APP

                        stop();
                        // TODOexist .. exist the APP
                        ((Node) event.getSource()).getScene().getWindow().hide();
                    });
                    Scene scene = new Scene(demoView.getView());
                    stage.setScene(scene);
                    stage.initOwner(stage.getOwner());
                    stage.show();
                });
            } else {
                notifyPreloader(new Preloader.ProgressNotification(0.1));
                MessageNotification notification = new MessageNotification(String.valueOf(200), application);
                notifyPreloader(notification);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.stop();
            }

        }

        public synchronized void stop() {
            running = false;
        }

    }

}
